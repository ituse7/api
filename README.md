# MeetMe API

 [![pipeline status](https://gitlab.com/ituse7/api/badges/master/pipeline.svg)](https://gitlab.com/ituse7/api/-/pipelines)
 [![coverage report](https://gitlab.com/ituse7/api/badges/master/coverage.svg)](https://ituse7.gitlab.io/api/)

MeetMe API for managing calendar bookings

---

## Install
Project requires [Poetry](https://python-poetry.org/) package management tool for installing dependencies.

```bash
# Clone the project
$ git clone https://gitlab.com/ituse7/api.git meetmeapi
# Go to the project directory
$ cd meetmeapi
# Install project requirements. Project supports Python ^3.8
# For fish shell: eval python3.9 '(which poetry)' install
$ python3.9 "$(which poetry)" install
# Activate the virtual environment
$ poetry shell
# Create a configuration file from the template
$ cp .env.template .env
# Migrate the database
$ aerich upgrade
# Test the application
$ pytest .
# Run the server with reload for development
$ uvicorn app.main:app --reload
```

## Configuration

Configuration for the project is managed by the `.env` file.
Check the template file `.env.template` for the possible configuration options.

Use `openssl rand -hex 32` command to generate a `SECRET_KEY` value.

---

&copy; 2020 Emin Mastizada, Aisenur Yoldas, Anil Ozlu, Halit Yagar, Nada Malek. MIT Licenced.
