"""
Documentations related to OpenAPI docs.

These are usually Markdown text files that are directly used in the OpenAPI specifications for better documentation.
"""
