from pydantic import BaseModel, Field


class CommonExceptionSchema(BaseModel):
    message: str = Field(None, title="message", description="Description for the error.")
    code: str = Field(None, title="code", description="Code for the error.")

    class Config:
        title = "Common exception"
