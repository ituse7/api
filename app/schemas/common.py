from typing import Optional

from pydantic import AnyHttpUrl, BaseModel, Field


class HomepageResponseSchema(BaseModel):
    name: str = Field("MeetMe API", description="Project name.")
    docs: str = Field("/docs/", description="Documentation URL.")
    repository: AnyHttpUrl = Field("https://gitlab.com/ituse7/api", description="Source code repository URL.")


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenPayload(BaseModel):
    sub: Optional[int] = None
