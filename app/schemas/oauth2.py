from typing import List, Optional

from pydantic import BaseModel, EmailStr, Field


class OAuth2PasswordRequestSchema(BaseModel):
    grant_type: Optional[str] = Field("password", description="Password grant type.")
    username: EmailStr = Field(..., description="Email address of the user.")
    password: str = Field(..., description="Password of the user for the provided email address.")
    scope: List[str] = Field([], description="List of API access scopes.")
    client_id: Optional[str] = Field(None, description="Client ID.")
    client_secret: Optional[str] = Field(None, description="Client secret.")
