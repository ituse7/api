from pydantic import BaseModel, EmailStr, Field
from tortoise.contrib.pydantic import pydantic_model_creator

from app.models import User

User_Pydantic = pydantic_model_creator(User, name="User")


class UserCreateSchema(BaseModel):
    email: EmailStr = Field(..., description="Email address for notifications and login.")
    password: str = Field(..., description="Password of the user.")
    name: str = Field(None, description="Full name of the user.")
