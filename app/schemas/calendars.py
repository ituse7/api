from datetime import datetime, time
from typing import Dict, Optional, Union

import pytz
from pydantic import BaseModel, EmailStr, Field, root_validator, validator
from tortoise.contrib.pydantic import pydantic_model_creator

from app.models import Calendar, Meeting

Calendar_Pydantic = pydantic_model_creator(Calendar, name="Calendar")
Meeting_Pydantic = pydantic_model_creator(Meeting, name="Meeting")

CalendarPublicSchema = pydantic_model_creator(
    Calendar, name="Public Calendar", exclude=("user", "rest_duration", "is_default", "created_at")
)

CalendarPublicBusySpotsSchema = pydantic_model_creator(
    Meeting,
    name="Public Calendar Busy Spots",
    exclude=(
        "id",
        "calendar",
        "fullname",
        "email",
        "subject",
        "notes",
        "approved",
        "meeting_url",
        "is_rest_time",
        "created_at",
    ),
)


# noinspection PyMethodParameters
class CalendarCreateSchema(BaseModel):
    name: str = Field(..., description="Name of the calendar.")
    slug: str = Field(..., description="URL slug of the calendar.")
    open_time: str = Field(default="09:00", description="Booking start time as HH:MM format.")
    close_time: str = Field(default="17:00", description="Booking end time as HH:MM format.")
    work_days: str = Field(
        default="Monday,Tuesday,Wednesday,Thursday,Friday", description="Work days like Monday, Tuesday, Friday."
    )
    timezone: str = Field(default="Europe/Istanbul", description="Calendar timezone.")
    default_duration: int = Field(default=30, description="Default meeting duration in minutes.")
    rest_duration: int = Field(default=15, description="Default rest duration between meetings in minutes.")
    is_default: bool = Field(default=True, description="User can only have one default calendar.")

    @validator("open_time")
    def verify_open_time_string(cls, v: str) -> str:
        v = v.strip()
        time.fromisoformat(v)  # will raise a value error with a description
        return v

    @validator("close_time")
    def verify_close_time_string(cls, v: str) -> str:
        v = v.strip()
        time.fromisoformat(v)  # will raise a value error with a description
        return v

    @validator("timezone")
    def check_timezone_set(cls, v: str) -> str:
        v = v.strip()
        if v not in pytz.all_timezones:
            raise ValueError("must be a valid timezone")
        return v


# noinspection PyMethodParameters
class MeetingCreateSchema(BaseModel):
    calendar_slug: str = Field(..., description="Slug of the calendar.")
    meeting_start: datetime = Field(..., description="Start time of the meeting with a timezone.")
    meeting_end: datetime = Field(..., description="End time of the meeting with a timezone.")
    fullname: str = Field(..., max_length=254, description="Full name of the guest.")
    email: EmailStr = Field(..., description="Email address of the guest.")
    subject: str = Field(..., max_length=254, description="Subject of the meeting.")
    notes: Optional[str] = Field(None, description="Notes related to the meeting.")

    @validator("meeting_start")
    def verify_start_time(cls, v: datetime) -> datetime:
        if v.tzinfo is None:
            raise ValueError("missing a timezone value")
        return v

    @validator("meeting_end")
    def verify_end_time(cls, v: datetime) -> datetime:
        if v.tzinfo is None:
            raise ValueError("missing a timezone value")
        return v

    @root_validator()
    def values_start_end_timing(cls, values: Dict[str, Optional[Union[str, datetime, EmailStr]]]) -> dict:
        start: datetime = values.get("meeting_start")  # type: ignore
        end: datetime = values.get("meeting_end")  # type: ignore
        if end < start:
            raise ValueError("meeting cannot end before it starts")
        return values


# noinspection PyMethodParameters
class CalendarBusySpotsRequestSchema(BaseModel):
    start_time: datetime = Field(..., description="Start time of the query.")
    end_time: datetime = Field(..., description="End time of the query.")

    @validator("start_time")
    def verify_start_time(cls, v: datetime) -> datetime:
        if v.tzinfo is None:
            raise ValueError("missing a timezone value")
        return v

    @validator("end_time")
    def verify_end_time(cls, v: datetime) -> datetime:
        if v.tzinfo is None:
            raise ValueError("missing a timezone value")
        return v


# class CalendarPublicBusySpotsSchema(BaseModel):
#     meeting_start: datetime = Field(..., description="Start time of a scheduled event.")
#     meeting_end: datetime = Field(..., description="End time of a scheduled event.")
