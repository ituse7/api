from typing import List

from pydantic import BaseModel, EmailStr


class EmailMessage(BaseModel):
    to: List[EmailStr]
    message: str
