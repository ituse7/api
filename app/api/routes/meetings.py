from datetime import datetime, time, timedelta
from typing import List, Optional

from fastapi import APIRouter, Depends
from tortoise.exceptions import DoesNotExist
from tortoise.query_utils import Q

from app.core.dependencies import get_current_active_user
from app.models import Calendar, Meeting, User
from app.schemas.calendars import Meeting_Pydantic, MeetingCreateSchema
from app.schemas.exceptions import CommonExceptionSchema
from app.utils.exceptions import (
    DoesNotExistsException,
    MeetingSlotBusyException,
    MeetingTimeException,
    shared_response_types,
)
from app.utils.time_conversions import convert_to_aware_time, get_timezone_from_date, get_utc_naive_time

meeting_router = APIRouter(tags=["meetings"])


@meeting_router.get(
    "/",
    response_model=List[Meeting_Pydantic],  # type: ignore
    summary="List of user's meetings",
    description="List of all meetings related to the user.",
    responses={
        400: shared_response_types["inactive_user"],
        401: shared_response_types["invalid_credential"],
        404: {"model": CommonExceptionSchema, "description": "Calendar with the provided slug was not found."},
    },
)
async def get_meetings(
    calendar_slug: Optional[str] = None,
    skip: int = 0,
    limit: int = 100,
    current_user: User = Depends(get_current_active_user),
):
    if calendar_slug:
        # check if calendar exists and filter for the calendar
        try:
            calendar = await Calendar.get(slug=calendar_slug, user=current_user)
        except DoesNotExist:
            raise DoesNotExistsException("Calendar", "slug", calendar_slug)
        meetings = Meeting.filter(calendar=calendar).distinct().offset(skip).limit(limit)
    else:
        meetings = Meeting.filter(calendar__user=current_user).distinct().offset(skip).limit(limit)
    return await Meeting_Pydantic.from_queryset(meetings)


@meeting_router.post(
    "/",
    response_model=Meeting_Pydantic,
    summary="Book a meeting",
    description="Create a new meeting for the calendar.",
    responses={
        400: shared_response_types["inactive_user"],
        401: shared_response_types["invalid_credential"],
        404: {"model": CommonExceptionSchema, "description": "Calendar with the provided slug was not found."},
        409: {
            "model": CommonExceptionSchema,
            "description": "Meeting time is outside of the work hours of the calendar (code: outside_work_hours) "
            "or there is another meeting in this time slot (code: busy_slot).",
        },
    },
)
async def create_new_meeting(new_meeting: MeetingCreateSchema) -> Meeting_Pydantic:  # type: ignore
    # check the calendar slug
    calendar = await Calendar.filter(slug=new_meeting.calendar_slug).select_related("user").first()
    if not calendar:
        raise DoesNotExistsException("Calendar", "slug", new_meeting.calendar_slug)

    # find the timezone of the user
    timezone = await get_timezone_from_date(new_meeting.meeting_start)
    # convert times to utc
    start_time = await get_utc_naive_time(new_meeting.meeting_start)
    end_time = await get_utc_naive_time(new_meeting.meeting_end)

    # prepare information about calendar's time
    calendar_start = await convert_to_aware_time(new_meeting.meeting_start, calendar.timezone)
    calendar_end = await convert_to_aware_time(new_meeting.meeting_end, calendar.timezone)
    # remove tzinfo from calendar times after converting time itself
    calendar_start = calendar_start.replace(tzinfo=None)
    calendar_end = calendar_end.replace(tzinfo=None)
    # also prepare work hours for the calendar
    calendar_open = datetime.combine(calendar_start.date(), time.fromisoformat(calendar.open_time))
    calendar_close = datetime.combine(calendar_start.date(), time.fromisoformat(calendar.close_time))

    # add 1 day to the end time if it crosses overnight
    if calendar_close < calendar_open:
        calendar_close += timedelta(days=1)

    # check if the new meeting is in work hours
    if calendar_start < calendar_open or calendar_end > calendar_close:
        raise MeetingTimeException()

    # check if the desired meeting time is available
    if await Meeting.filter(
        Q(calendar=calendar)
        & (Q(meeting_start__range=(start_time, end_time)) | Q(meeting_end__range=(start_time, end_time)))
    ).exists():
        raise MeetingSlotBusyException()

    # create the meeting
    meeting = await Meeting.create(
        calendar=calendar,
        meeting_start=start_time,
        meeting_end=end_time,
        timezone=timezone,
        fullname=new_meeting.fullname,
        email=new_meeting.email,
        subject=new_meeting.subject,
        notes=new_meeting.notes,
        approved=False,
    )
    # create a leg time
    if calendar.rest_duration:
        await Meeting.create(
            calendar=calendar,
            meeting_start=end_time,
            meeting_end=end_time + timedelta(minutes=calendar.rest_duration),
            timezone=timezone,
            fullname=calendar.user.name,
            email=calendar.user.email,
            subject="Leg time",
            approved=False,
            is_rest_time=True,
        )
    return Meeting_Pydantic.from_orm(meeting)
