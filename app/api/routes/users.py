from datetime import timedelta
from typing import List

from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm
from tortoise.exceptions import DoesNotExist

from app.core.dependencies import get_current_active_user, get_current_staff_user
from app.core.settings import settings
from app.models import User
from app.schemas.common import Token
from app.schemas.exceptions import CommonExceptionSchema
from app.schemas.oauth2 import OAuth2PasswordRequestSchema
from app.schemas.users import User_Pydantic, UserCreateSchema
from app.utils.exceptions import (
    DoesNotExistsException,
    DuplicatedEmailException,
    InactiveUserException,
    IncorrectCredentialsException,
    RegistrationClosedException,
    shared_response_types,
)
from app.utils.security import create_access_token, get_password_hash, verify_password

user_router = APIRouter(tags=["users"])


@user_router.get(
    "/me/",
    response_model=User_Pydantic,
    summary="Current user details",
    description="Details about the current user.",
    responses={
        400: shared_response_types["inactive_user"],
        401: shared_response_types["invalid_credential"],
    },
)
async def get_current_user(current_user: User = Depends(get_current_active_user)) -> User:
    return current_user


@user_router.get(
    "/",
    response_model=List[User_Pydantic],  # type: ignore
    summary="List of users",
    description="List of all users, this endpoint can only be accessed with a staff credentials.",
    responses={
        400: shared_response_types["inactive_user"],
        401: shared_response_types["invalid_credential"],
        403: shared_response_types["permission_denied"],
    },
)
async def get_users(
    skip: int = 0, limit: int = 100, _: User = Depends(get_current_staff_user)
) -> List[User_Pydantic]:  # type: ignore
    return await User_Pydantic.from_queryset(User.filter().offset(skip).limit(limit))


@user_router.post(
    "/",
    response_model=User_Pydantic,
    summary="Register new users",
    description="Register a new user.",
    responses={
        400: {"model": CommonExceptionSchema, "description": "Email address is already registered."},
        409: {"model": CommonExceptionSchema, "description": "Registration is closed."},
    },
)
async def register(new_user: UserCreateSchema) -> User_Pydantic:  # type: ignore
    if not settings.ALLOW_REGISTRATION:
        raise RegistrationClosedException()
    if await User.filter(email=new_user.email).exists():
        raise DuplicatedEmailException(email=new_user.email)
    password_hash = get_password_hash(new_user.password)
    user = await User.create(
        name=new_user.name,
        email=new_user.email,
        password=password_hash,
    )
    return User_Pydantic.from_orm(user)


@user_router.post(
    "/access-token/",
    response_model=Token,
    summary="Get access token for a user",
    description="Get an access token for the user to use in the Authorization header.",
    responses={
        400: {"model": CommonExceptionSchema, "description": "Incorrect credentials or inactive user."},
        404: {"model": CommonExceptionSchema, "description": "User with that email address is not registered."},
    },
)
async def login_access_token(auth_data: OAuth2PasswordRequestSchema) -> dict:
    """OAuth2 compliant token generator using a json data."""
    try:
        user = await User.get(email=auth_data.username)
    except DoesNotExist:
        raise DoesNotExistsException("User", "email", auth_data.username)
    if not verify_password(auth_data.password, user.password):
        raise IncorrectCredentialsException()
    if not user.is_active:
        raise InactiveUserException(auth_data.username)
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    return {
        "access_token": create_access_token(user.id, expires_delta=access_token_expires),
        "token_type": "bearer",
    }


@user_router.post(
    "/form-token/",
    response_model=Token,
    summary="Get access token using a form data",
    description="OAuth2 compliant token generator using a form.",
    responses={
        400: {"model": CommonExceptionSchema, "description": "Incorrect credentials or inactive user."},
        404: {"model": CommonExceptionSchema, "description": "User with that email address is not registered."},
    },
)
async def form_login_access_token(form_data: OAuth2PasswordRequestForm = Depends()) -> dict:
    """OAuth2 compliant token generator using a form."""
    try:
        user = await User.get(email=form_data.username)
    except DoesNotExist:
        raise DoesNotExistsException("User", "email", form_data.username)
    if not verify_password(form_data.password, user.password):
        raise IncorrectCredentialsException()
    if not user.is_active:
        raise InactiveUserException(form_data.username)
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    return {
        "access_token": create_access_token(user.id, expires_delta=access_token_expires),
        "token_type": "bearer",
    }
