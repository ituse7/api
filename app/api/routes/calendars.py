from typing import List

from fastapi import APIRouter, Depends
from tortoise.exceptions import DoesNotExist

from app.core.dependencies import get_current_active_user
from app.models import Calendar, Meeting, User
from app.schemas.calendars import (
    Calendar_Pydantic,
    CalendarBusySpotsRequestSchema,
    CalendarCreateSchema,
    CalendarPublicBusySpotsSchema,
    CalendarPublicSchema,
)
from app.schemas.exceptions import CommonExceptionSchema
from app.utils.exceptions import CalendarExistsException, DoesNotExistsException, shared_response_types
from app.utils.time_conversions import get_utc_naive_time

calendar_router = APIRouter(tags=["calendars"])


@calendar_router.get(
    "/",
    response_model=List[Calendar_Pydantic],  # type: ignore
    summary="List of calendars",
    description="List of all calendars of the user.",
    responses={
        400: shared_response_types["inactive_user"],
        401: shared_response_types["invalid_credential"],
    },
)
async def get_calendars(
    skip: int = 0, limit: int = 100, current_user: User = Depends(get_current_active_user)
) -> List[Calendar_Pydantic]:  # type: ignore
    return await Calendar_Pydantic.from_queryset(Calendar.filter(user=current_user).offset(skip).limit(limit))


@calendar_router.post(
    "/",
    response_model=Calendar_Pydantic,
    summary="Create a calendar",
    description="Create a new calendar.",
    responses={
        400: shared_response_types["inactive_user"],
        401: shared_response_types["invalid_credential"],
        409: {"model": CommonExceptionSchema, "description": "A calendar with the provided slug already exists."},
    },
)
async def create_new_calendar(
    new_calendar: CalendarCreateSchema, current_user: User = Depends(get_current_active_user)
) -> Calendar_Pydantic:  # type: ignore
    # check to make sure the slug is unique
    if await Calendar.filter(slug=new_calendar.slug).exists():
        raise CalendarExistsException(slug=new_calendar.slug)
    # first calendar is always the default
    if not await Calendar.filter(user=current_user, is_default=True).exists():
        new_calendar.is_default = True
    # create the calendar
    calendar = await Calendar.create(
        user=current_user,
        name=new_calendar.name,
        slug=new_calendar.slug,
        open_time=new_calendar.open_time,
        close_time=new_calendar.close_time,
        work_days=new_calendar.work_days,
        timezone=new_calendar.timezone,
        default_duration=new_calendar.default_duration,
        rest_duration=new_calendar.rest_duration,
        is_default=new_calendar.is_default,
    )
    if calendar.is_default:
        # make all other calendars non-default
        await Calendar.filter(user=current_user).exclude(slug=calendar.slug).update(is_default=False)
    return Calendar_Pydantic.from_orm(calendar)


@calendar_router.get(
    "/default/",
    response_model=Calendar_Pydantic,
    summary="Default user calendar",
    description="The default calendar for the current user.",
    responses={
        400: shared_response_types["inactive_user"],
        401: shared_response_types["invalid_credential"],
        404: {"model": CommonExceptionSchema, "description": "There is no default calendar for the current user."},
    },
)
async def get_default_calendar(
    current_user: User = Depends(get_current_active_user),
) -> Calendar_Pydantic:  # type: ignore
    calendar = await Calendar.filter(user=current_user, is_default=True).first()
    if not calendar:
        raise DoesNotExistsException("Calendar", "default value", "true")
    return Calendar_Pydantic.from_orm(calendar)


@calendar_router.get(
    "/{slug}/",
    response_model=CalendarPublicSchema,
    summary="Public calendar information",
    description="Get details about the calendar using its slug value.",
    responses={
        404: {"model": CommonExceptionSchema, "description": "A calendar with the requested slug does not exist."},
    },
)
async def get_public_calendar_info(slug: str) -> CalendarPublicSchema:  # type: ignore
    try:
        calendar = await Calendar.get(slug=slug)
    except DoesNotExist:
        raise DoesNotExistsException("Calendar", "slug", slug)
    return CalendarPublicSchema.from_orm(calendar)


@calendar_router.get(
    "/{slug}/spots/",
    response_model=List[CalendarPublicBusySpotsSchema],  # type: ignore
    summary="Calendar busy spots",
    description="Get busy spots of the calendar for the provided date range.",
    responses={
        404: {"model": CommonExceptionSchema, "description": "A calendar with the requested slug does not exist."},
    },
)
async def get_public_calendar_busy_spots_info(
    slug: str, date_filter: CalendarBusySpotsRequestSchema, skip: int = 0, limit: int = 100
) -> List[CalendarPublicBusySpotsSchema]:  # type: ignore
    try:
        calendar = await Calendar.get(slug=slug)
    except DoesNotExist:
        raise DoesNotExistsException("Calendar", "slug", slug)

    # convert times to utc
    start_time = await get_utc_naive_time(date_filter.start_time)
    end_time = await get_utc_naive_time(date_filter.end_time)

    # get list of meetings
    meetings = (
        Meeting.filter(calendar=calendar, meeting_start__gte=start_time, meeting_end__lte=end_time)
        .offset(skip)
        .limit(limit)
    )
    return await CalendarPublicBusySpotsSchema.from_queryset(meetings)
