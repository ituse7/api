from fastapi import APIRouter
from fastapi.responses import JSONResponse

from app.schemas.common import HomepageResponseSchema

api_router = APIRouter()


@api_router.get("/", response_model=HomepageResponseSchema, description="Basic details related to the API.")
async def index() -> JSONResponse:
    return JSONResponse(status_code=200, content=HomepageResponseSchema().dict())
