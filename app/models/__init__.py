"""Due to the limitations of the Aerich migration manager, all models should be in a single file."""
from datetime import datetime
from typing import Optional

from tortoise import fields, models


class User(models.Model):
    """User model for registration/login and associated objects."""

    id: int = fields.IntField(pk=True)
    email: str = fields.CharField(
        max_length=254, unique=True, index=True, description="Email address for notifications and login."
    )
    password: str = fields.CharField(max_length=254, description="Password of the user.")
    name: str = fields.CharField(max_length=254, null=True, description="Full name of the user.")
    is_active: bool = fields.BooleanField(default=True, description="Allow/Disallow login.")  # type: ignore
    is_superuser: bool = fields.BooleanField(default=False, description="Admin user status.")  # type: ignore

    modified_at: Optional[datetime] = fields.DatetimeField(
        auto_now=True, null=True, description="Profile update timestamp."
    )
    created_at: datetime = fields.DatetimeField(auto_now_add=True, description="Profile creation timestamp.")

    class Meta:
        ordering = ["id"]

    class PydanticMeta:
        exclude = ["id", "password"]

    def __str__(self) -> str:
        return self.name


class Calendar(models.Model):
    """Calendar settings."""

    id: int = fields.IntField(pk=True)
    user: User = fields.ForeignKeyField(
        "models.User", related_name="calendars", on_delete=fields.CASCADE, description="The owner of the calendar."
    )  # type: ignore
    name: str = fields.CharField(max_length=250, description="Name of the calendar.")
    slug: str = fields.CharField(max_length=50, description="URL slug of the calendar.", unique=True)
    open_time: str = fields.CharField(max_length=5, description="Booking start time as HH:MM format.")
    close_time: str = fields.CharField(max_length=5, description="Booking end time as HH:MM format.")
    work_days: str = fields.CharField(max_length=56, description="Work days like Monday, Tuesday, Friday.")
    timezone: str = fields.CharField(max_length=35, description="Calendar timezone.")
    default_duration: int = fields.IntField(default=30, description="Default meeting duration in minutes.")
    rest_duration: int = fields.IntField(default=15, description="Default rest duration between meetings in minutes.")
    is_default: bool = fields.BooleanField(
        default=True, description="User can only have one default calendar."
    )  # type: ignore
    created_at: datetime = fields.DatetimeField(auto_now_add=True, description="Calendar creation timestamp.")

    class Meta:
        ordering = ["-created_at"]

    class PydanticMeta:
        exclude = ["id"]

    def __str__(self) -> str:
        return self.name


class Meeting(models.Model):
    """A calendar meeting."""

    id: int = fields.IntField(pk=True)
    calendar: Calendar = fields.ForeignKeyField(
        "models.Calendar",
        related_name="meetings",
        on_delete=fields.CASCADE,
        description="Related calendar of the meeting.",
    )  # type: ignore
    meeting_start: datetime = fields.DatetimeField(description="Start time of the meeting.")
    meeting_end: datetime = fields.DatetimeField(description="End time of the meeting.")
    timezone: str = fields.CharField(max_length=35, description="Guest's timezone.")
    fullname: str = fields.CharField(max_length=254, description="Full name of the guest.")
    email: str = fields.CharField(max_length=254, description="Email address of the guest.")
    subject: str = fields.CharField(max_length=254, description="Subject of the meeting.")
    notes: Optional[str] = fields.TextField(null=True, description="Notes related to the meeting.")
    approved: bool = fields.BooleanField(
        default=False, description="Is the meeting approved by the calendar owner?"
    )  # type: ignore
    meeting_url: Optional[str] = fields.TextField(null=True, description="URL address of the meeting.")
    is_rest_time: bool = fields.BooleanField(
        default=False, description="Mark the meeting as a leg time between meetings."
    )  # type: ignore
    created_at: datetime = fields.DatetimeField(auto_now_add=True, description="Meeting booking timestamp.")

    class Meta:
        ordering = ["-created_at"]

    class PydanticMeta:
        exclude = ["id", "is_rest_time"]

    def __str__(self) -> str:
        return str(self.id)
