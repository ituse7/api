from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from tortoise.contrib.fastapi import register_tortoise

from app import __version__
from app.api.routes import api_router
from app.api.routes.calendars import calendar_router
from app.api.routes.meetings import meeting_router
from app.api.routes.users import user_router
from app.core.events import start_app_handler, stop_app_handler
from app.core.orm import TORTOISE_ORM
from app.core.settings import settings
from app.utils.exceptions import register_exception_handlers
from app.utils.meta import api_description, tags_metadata


def get_application() -> FastAPI:
    application = FastAPI(
        title="MeetMe API",
        description=api_description,
        debug=settings.DEBUG,
        version=__version__,
        openapi_url="/openapi.json",
        openapi_tags=tags_metadata,
    )

    # CORS middleware
    application.add_middleware(
        CORSMiddleware,
        allow_origins=settings.CORS_ORIGINS or ["*"],  # type: ignore
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    # startup and shutdown events
    application.add_event_handler("startup", start_app_handler(application))
    application.add_event_handler("shutdown", stop_app_handler(application))

    # add exception handlers
    register_exception_handlers(application)

    # there will be single version for this app, versioning can be added later
    application.include_router(api_router, prefix="", tags=["core"])
    application.include_router(user_router, prefix="/users", tags=["users"])
    application.include_router(calendar_router, prefix="/calendars", tags=["calendars"])
    application.include_router(meeting_router, prefix="/meetings", tags=["meetings"])

    return application


app = get_application()
register_tortoise(
    app,
    config=TORTOISE_ORM,
    generate_schemas=False,
    add_exception_handlers=True,
)
