from datetime import datetime
from datetime import timezone as dt_timezone
from datetime import tzinfo
from typing import Optional

import pytz
from pytz.exceptions import UnknownTimeZoneError

WEEKDAYS = {0: "Monday", 1: "Tuesday", 2: "Wednesday", 3: "Thursday", 4: "Friday", 5: "Saturday", 6: "Sunday"}


# noinspection PyProtectedMember
async def get_possible_timezone_value(tz: dt_timezone) -> Optional[str]:
    now = datetime.now(pytz.utc)
    for _tz in map(pytz.timezone, pytz.all_timezones_set):
        dt = now.astimezone(_tz)
        if hasattr(dt.tzinfo, "_utcoffset") and dt_timezone(getattr(dt.tzinfo, "_utcoffset")) == tz:
            return _tz.zone
    return None


async def get_timezone_from_date(value: datetime) -> Optional[str]:
    """Get timezone of the datetime value as a pytz compatible string."""
    if value.tzinfo is not None:
        if value.tzinfo == dt_timezone.utc:
            return "UTC"
        if hasattr(value.tzinfo, "zone"):
            return getattr(value.tzinfo, "zone")
        # timezone with a timedelta
        if isinstance(value.tzinfo, dt_timezone) and hasattr(value, "tzinfo"):
            return await get_possible_timezone_value(getattr(value, "tzinfo"))
    return None


async def get_timezone_info(timezone: str) -> Optional[tzinfo]:
    """Get timezone using the string value of the location."""
    try:
        return pytz.timezone(timezone)
    except UnknownTimeZoneError:
        return None


async def get_current_time(timezone: str) -> datetime:
    """Get current date and time in the desired timezone."""
    tz = await get_timezone_info(timezone)
    if not tz:
        return datetime.now(tz=pytz.utc)
    return datetime.now(tz=tz)


async def current_week_day(timezone: str) -> str:
    """Get current week day, 0: Monday, 6: Sunday."""
    now = await get_current_time(timezone)
    return WEEKDAYS[now.weekday()]


async def get_utc_naive_time(value: datetime) -> datetime:
    """Get UTC value for the datetime without any timezone info."""
    return value.astimezone(pytz.utc).replace(tzinfo=None)


async def convert_to_aware_time(value: datetime, timezone: str) -> datetime:
    """Convert datetime to a desired timezone."""
    tz = await get_timezone_info(timezone)
    return value.astimezone(tz)
