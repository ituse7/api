"""
Various documentation related parameters for the application.
"""
from pathlib import Path
from typing import Any, Dict, List

BASE_DIR = Path(__file__).resolve().parent.parent


tags_metadata: List[Dict[str, Any]] = []


api_description: str = open(f"{BASE_DIR}/docs/API_DESCRIPTION.md", "r").read()
