from typing import Union

from fastapi import FastAPI, HTTPException, Request
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from pydantic import ValidationError
from starlette.exceptions import HTTPException as StarletteHTTPException
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY

from app.schemas.exceptions import CommonExceptionSchema


class InactiveUserException(Exception):
    """User account is disabled (is_active=False)."""

    def __init__(self, email: str):
        self.email = email


class UnAuthorizedException(Exception):
    """OAuth2 token is incorrect."""

    pass


class PermissionDeniedException(Exception):
    """User do not have permission for this resource."""

    pass


class RegistrationClosedException(Exception):
    pass


class DuplicatedEmailException(Exception):
    def __init__(self, email: str):
        self.email = email


class IncorrectCredentialsException(Exception):
    """Login credentials are not correct or user is disabled."""

    pass


class CalendarExistsException(Exception):
    """Calendar with the chosen slug already exists."""

    def __init__(self, slug: str):
        self.slug = slug


class MeetingTimeException(Exception):
    """The start or end time of a meeting is not during the work hours of the calendar."""

    pass


class MeetingSlotBusyException(Exception):
    """The meeting has a time conflict with an existing meeting."""

    pass


class DoesNotExistsException(Exception):
    """Model with a key=value does not exist."""

    def __init__(self, object_name: str, key: str, value: str):
        self.object_name = object_name
        self.key = key
        self.value = value


async def general_http_exception_handler(
    _: Request, exc: Union[HTTPException, StarletteHTTPException]
) -> JSONResponse:
    code = "not_found" if exc.status_code == 404 else "http_exception"
    return JSONResponse(status_code=exc.status_code, content={"message": exc.detail, "code": code})


async def general_validation_exception_handler(
    _: Request, exc: Union[RequestValidationError, ValidationError]
) -> JSONResponse:
    return JSONResponse(
        status_code=HTTP_422_UNPROCESSABLE_ENTITY,
        content={"detail": jsonable_encoder(exc.errors())},
    )


async def inactive_user_exception(_: Request, exc: InactiveUserException) -> JSONResponse:
    return JSONResponse(
        status_code=400, content={"message": f"User account {exc.email} is not active", "code": "inactive_user"}
    )


async def un_authorized_exception(_: Request, __: UnAuthorizedException) -> JSONResponse:
    return JSONResponse(
        status_code=401,
        content={"message": "Invalid authentication credentials", "code": "invalid_credentials"},
        headers={"WWW-Authenticate": "Bearer"},
    )


async def permission_denied_exception(_: Request, __: PermissionDeniedException) -> JSONResponse:
    return JSONResponse(
        status_code=403,
        content={
            "message": "The authenticated user is not permitted to perform the requested operation.",
            "code": "permission_denied",
        },
    )


async def registration_closed_exception(_: Request, __: RegistrationClosedException) -> JSONResponse:
    return JSONResponse(status_code=409, content={"message": "Registration is closed.", "code": "registration_closed"})


async def duplicated_email_exception(_: Request, exc: DuplicatedEmailException) -> JSONResponse:
    return JSONResponse(
        status_code=400,
        content={"message": f"User with email address {exc.email} already exists.", "code": "duplicated_email"},
    )


async def incorrect_credentials_exception(_: Request, __: IncorrectCredentialsException) -> JSONResponse:
    return JSONResponse(status_code=400, content={"message": "Incorrect credentials", "code": "incorrect_credentials"})


async def calendar_exists_exception(_: Request, exc: CalendarExistsException) -> JSONResponse:
    return JSONResponse(
        status_code=409,
        content={"message": f"A calendar with slug {exc.slug} already exists.", "code": "calendar_exists"},
    )


async def meeting_time_exception(_: Request, __: MeetingTimeException) -> JSONResponse:
    return JSONResponse(
        status_code=409,
        content={
            "message": "Meeting time is outside of the work hours of the calendar.",
            "code": "outside_work_hours",
        },
    )


async def meeting_slot_busy_exception(_: Request, __: MeetingSlotBusyException) -> JSONResponse:
    return JSONResponse(
        status_code=409,
        content={"message": "The meeting has a time conflict with an existing meeting.", "code": "busy_slot"},
    )


async def does_not_exists_exception(_: Request, exc: DoesNotExistsException) -> JSONResponse:
    return JSONResponse(
        status_code=404,
        content={"message": f"{exc.object_name} with {exc.key} {exc.value} not found.", "code": "not_found"},
    )


shared_response_types = {
    "inactive_user": {"model": CommonExceptionSchema, "description": "Inactive user account."},
    "invalid_credential": {"model": CommonExceptionSchema, "description": "Invalid authentication credentials."},
    "permission_denied": {
        "model": CommonExceptionSchema,
        "description": "The authenticated user is not permitted to perform the requested operation.",
    },
}


CUSTOM_EXCEPTIONS = [
    (HTTPException, general_http_exception_handler),
    (StarletteHTTPException, general_http_exception_handler),
    (RequestValidationError, general_validation_exception_handler),
    (ValidationError, general_validation_exception_handler),
    (InactiveUserException, inactive_user_exception),
    (UnAuthorizedException, un_authorized_exception),
    (PermissionDeniedException, permission_denied_exception),
    (RegistrationClosedException, registration_closed_exception),
    (DuplicatedEmailException, duplicated_email_exception),
    (IncorrectCredentialsException, incorrect_credentials_exception),
    (CalendarExistsException, calendar_exists_exception),
    (MeetingTimeException, meeting_time_exception),
    (MeetingSlotBusyException, meeting_slot_busy_exception),
    (DoesNotExistsException, does_not_exists_exception),
]


def register_exception_handlers(app: FastAPI) -> None:
    for exception_type in CUSTOM_EXCEPTIONS:
        app.add_exception_handler(exception_type[0], exception_type[1])  # type: ignore
