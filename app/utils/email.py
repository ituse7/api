from typing import Optional

from aiosmtplib import send

from app.core.settings import settings
from app.schemas.email import EmailMessage


async def send_email(message: EmailMessage, sender: Optional[str] = None) -> None:
    sender = sender or settings.EMAIL_DEFAULT_SENDER
    await send(
        message=message.message,
        sender=sender,
        recipients=message.to,
        hostname=settings.EMAIL_HOSTNAME,
        port=settings.EMAIL_PORT,
        username=settings.EMAIL_USERNAME,
        password=settings.EMAIL_PASSWORD.get_secret_value(),
        use_tls=settings.EMAIL_USE_TLS,
        start_tls=settings.EMAIL_START_TLS,
    )
