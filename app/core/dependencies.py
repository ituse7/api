from fastapi import Depends
from fastapi.security import OAuth2PasswordBearer
from jose import jwt
from pydantic import ValidationError
from tortoise.exceptions import DoesNotExist

from app.core.settings import settings
from app.models import User
from app.schemas.common import TokenPayload
from app.utils.exceptions import InactiveUserException, PermissionDeniedException, UnAuthorizedException

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="users/form-token/")


async def get_current_user(token: str = Depends(oauth2_scheme)) -> User:
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.JWT_ALGORITHM])
        token_data = TokenPayload(**payload)
    except (jwt.JWTError, ValidationError):
        raise UnAuthorizedException()
    try:
        user = await User.get(id=token_data.sub)
    except DoesNotExist:
        raise UnAuthorizedException()
    return user


async def get_current_active_user(current_user: User = Depends(get_current_user)) -> User:
    if not current_user.is_active:
        raise InactiveUserException(current_user.email)
    return current_user


async def get_current_staff_user(current_user: User = Depends(get_current_user)) -> User:
    if not current_user.is_superuser:
        raise PermissionDeniedException()
    return current_user
