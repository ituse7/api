from typing import List, Union

from pydantic import AnyHttpUrl, BaseSettings, EmailStr, SecretStr, validator

from app.utils.meta import BASE_DIR


class Settings(BaseSettings):
    # essentials
    DEBUG: bool = False
    TEST_MODE: bool = False  # use test db
    SECRET_KEY: str
    # email settings
    EMAIL_DEFAULT_SENDER: EmailStr
    EMAIL_DEFAULT_CONTACT: EmailStr
    EMAIL_HOSTNAME: str
    EMAIL_PORT: int
    EMAIL_USERNAME: str
    EMAIL_PASSWORD: SecretStr
    EMAIL_USE_TLS: bool = True
    EMAIL_START_TLS: bool = False
    # database
    DATABASE_URI: str = f"sqlite:///{BASE_DIR}/../db.sqlite3"
    # allowed cors origins
    CORS_ORIGINS: List[AnyHttpUrl] = []
    # Predefined settings
    JWT_ALGORITHM: str = "HS256"
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 20160  # 14 days
    ALLOW_REGISTRATION: bool = True

    @validator("CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        """Parse and check allowed origins."""
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        env_prefix = ""
        case_sensitive = True


settings = Settings()
