from tortoise.backends.base.config_generator import expand_db_url

from app.core.settings import settings

TORTOISE_ORM = {
    "connections": {"default": expand_db_url(settings.DATABASE_URI, testing=settings.TEST_MODE)},
    "apps": {"models": {"models": ["app.models", "aerich.models"], "default_connection": "default"}},
}
