-- upgrade --
CREATE TABLE IF NOT EXISTS "user" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "email" VARCHAR(254) NOT NULL UNIQUE /* Email address for notifications and login. */,
    "password" VARCHAR(254) NOT NULL  /* Password of the user. */,
    "name" VARCHAR(254)   /* Full name of the user. */,
    "is_active" INT NOT NULL  DEFAULT 1 /* Allow\/Disallow login. */,
    "is_superuser" INT NOT NULL  DEFAULT 0 /* Admin user status. */,
    "modified_at" TIMESTAMP   DEFAULT CURRENT_TIMESTAMP /* Profile update timestamp. */,
    "created_at" TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP /* Profile creation timestamp. */
) /* User model for registration\/login and associated objects. */;
CREATE INDEX IF NOT EXISTS "idx_user_email_1b4f1c" ON "user" ("email");
CREATE TABLE IF NOT EXISTS "calendar" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "name" VARCHAR(250) NOT NULL  /* Name of the calendar. */,
    "slug" VARCHAR(50) NOT NULL UNIQUE /* URL slug of the calendar. */,
    "open_time" VARCHAR(5) NOT NULL  /* Booking start time as HH:MM format. */,
    "close_time" VARCHAR(5) NOT NULL  /* Booking end time as HH:MM format. */,
    "work_days" VARCHAR(56) NOT NULL  /* Work days like Monday, Tuesday, Friday. */,
    "timezone" VARCHAR(35) NOT NULL  /* Calendar timezone. */,
    "default_duration" INT NOT NULL  DEFAULT 30 /* Default meeting duration in minutes. */,
    "rest_duration" INT NOT NULL  DEFAULT 15 /* Default rest duration between meetings in minutes. */,
    "is_default" INT NOT NULL  DEFAULT 1 /* User can only have one default calendar. */,
    "created_at" TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP /* Calendar creation timestamp. */,
    "user_id" INT NOT NULL REFERENCES "user" ("id") ON DELETE CASCADE /* The owner of the calendar. */
) /* Calendar settings. */;
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(20) NOT NULL,
    "content" TEXT NOT NULL
);
