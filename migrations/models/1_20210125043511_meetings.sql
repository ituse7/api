-- upgrade --
CREATE TABLE IF NOT EXISTS "meeting" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "meeting_start" TIMESTAMP NOT NULL  /* Start time of the meeting. */,
    "meeting_end" TIMESTAMP NOT NULL  /* End time of the meeting. */,
    "timezone" VARCHAR(35) NOT NULL  /* Guest's timezone. */,
    "fullname" VARCHAR(254) NOT NULL  /* Full name of the guest. */,
    "email" VARCHAR(254) NOT NULL  /* Email address of the guest. */,
    "subject" VARCHAR(254) NOT NULL  /* Subject of the meeting. */,
    "notes" TEXT   /* Notes related to the meeting. */,
    "approved" INT NOT NULL  DEFAULT 0 /* Is the meeting approved by the calendar owner? */,
    "meeting_url" TEXT   /* URL address of the meeting. */,
    "is_rest_time" INT NOT NULL  DEFAULT 0 /* Mark the meeting as a leg time between meetings. */,
    "created_at" TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP /* Meeting booking timestamp. */,
    "calendar_id" INT NOT NULL REFERENCES "calendar" ("id") ON DELETE CASCADE /* Related calendar of the meeting. */
) /* A calendar meeting. */;
-- downgrade --
DROP TABLE IF EXISTS "meeting";
