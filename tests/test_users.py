# mypy: no-disallow-untyped-decorators

import asyncio

from fastapi.testclient import TestClient

from app.core.settings import settings
from tests.shared_utils import (
    TEST_USER_EMAIL,
    TEST_USER_NAME,
    TEST_USER_PASS,
    deactivate_user,
    delete_user,
    get_user_from_db,
    get_user_id_using_email,
    promote_user,
)


def test_create_user(client: TestClient, event_loop: asyncio.AbstractEventLoop) -> None:  # nosec
    # delete old user if it exists
    user_id = event_loop.run_until_complete(get_user_id_using_email(TEST_USER_EMAIL))
    if user_id:
        event_loop.run_until_complete(delete_user(user_id))
    # try to register with a missing parameter
    response = client.post("/users/", json={"email": TEST_USER_EMAIL, "name": TEST_USER_NAME})
    assert response.status_code == 422, response.text
    data = response.json()
    assert "detail" in data, data
    assert len(data["detail"]) == 1, data
    assert data["detail"][0]["type"] == "value_error.missing", data["detail"]
    # try with registration closed
    settings.ALLOW_REGISTRATION = False
    response = client.post(
        "/users/", json={"email": TEST_USER_EMAIL, "password": TEST_USER_PASS, "name": TEST_USER_NAME}
    )
    assert response.status_code == 409, response.text
    data = response.json()
    assert data["code"] == "registration_closed", data
    settings.ALLOW_REGISTRATION = True
    # register a test user
    response = client.post(
        "/users/", json={"email": TEST_USER_EMAIL, "password": TEST_USER_PASS, "name": TEST_USER_NAME}
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == TEST_USER_EMAIL, data
    assert "id" not in data, data
    user_id = event_loop.run_until_complete(get_user_id_using_email(data["email"]))
    assert user_id, "no user id returned"
    user_obj = event_loop.run_until_complete(get_user_from_db(user_id))
    assert user_obj.__str__() == TEST_USER_NAME
    # try with duplicate email address
    response = client.post(
        "/users/", json={"email": TEST_USER_EMAIL, "password": TEST_USER_PASS, "name": TEST_USER_NAME}
    )
    assert response.status_code == 400, response.text
    data = response.json()
    assert data["code"] == "duplicated_email", data
    # delete the test user
    event_loop.run_until_complete(delete_user(user_id))


def test_access_token(client: TestClient, event_loop: asyncio.AbstractEventLoop) -> None:
    response = client.post(
        "/users/", json={"email": TEST_USER_EMAIL, "password": TEST_USER_PASS, "name": TEST_USER_NAME}
    )
    assert response.status_code == 200, response.text
    # try to login with incorrect password
    response = client.post("/users/access-token/", json={"username": TEST_USER_EMAIL, "password": "NotAPassword"})
    assert response.status_code == 400, response.text
    data = response.json()
    assert data["code"] == "incorrect_credentials", data
    # try to login
    response = client.post("/users/access-token/", json={"username": TEST_USER_EMAIL, "password": TEST_USER_PASS})
    assert response.status_code == 200, response.text
    data = response.json()
    assert "access_token" in data, data
    assert "token_type" in data, data
    assert data["token_type"] == "bearer", data["token_type"]
    token = data["access_token"]
    # try profile details with incorrect token
    response = client.get("/users/me/", headers={"Authorization": "Bearer notACorrectToken"})
    assert response.status_code == 401, response.text
    data = response.json()
    assert data["code"] == "invalid_credentials"
    # get profile details
    response = client.get("/users/me/", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 200, response.text
    data = response.json()
    assert "id" not in data, data
    assert "email" in data, data
    assert data["email"] == TEST_USER_EMAIL, data["email"]
    user_id = event_loop.run_until_complete(get_user_id_using_email(data["email"]))
    assert user_id, "no user id returned"
    # try to login using the oauth2 form
    response = client.post("/users/form-token/", data={"username": TEST_USER_EMAIL, "password": TEST_USER_PASS})
    assert response.status_code == 200, response.text
    data = response.json()
    assert "access_token" in data, data
    assert "token_type" in data, data
    # form login with incorrect email
    response = client.post(
        "/users/form-token/", data={"username": "incorrect@email.domain", "password": TEST_USER_PASS}
    )
    assert response.status_code == 404, response.text
    data = response.json()
    assert data["code"] == "not_found", data
    # form login with incorrect password
    response = client.post("/users/form-token/", data={"username": TEST_USER_EMAIL, "password": "NotAPassword"})
    assert response.status_code == 400, response.text
    data = response.json()
    assert data["code"] == "incorrect_credentials", data
    # try to get list of users without superuser power
    response = client.get("/users/", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 403, response.text
    data = response.json()
    assert data["code"] == "permission_denied"
    # promote the user
    event_loop.run_until_complete(promote_user(user_id))
    response = client.get("/users/", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 200, response.text
    data = response.json()
    assert len(data)
    # deactivate the user
    event_loop.run_until_complete(deactivate_user(user_id))
    response = client.post("/users/access-token/", json={"username": TEST_USER_EMAIL, "password": TEST_USER_PASS})
    assert response.status_code == 400, response.text
    data = response.json()
    assert data["code"] == "inactive_user", data
    response = client.post("/users/form-token/", data={"username": TEST_USER_EMAIL, "password": TEST_USER_PASS})
    assert response.status_code == 400, response.text
    data = response.json()
    assert data["code"] == "inactive_user", data
    # test token of a deactivated user
    response = client.get("/users/me/", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 400, response.text
    data = response.json()
    assert data["code"] == "inactive_user", data
    # delete the test user
    event_loop.run_until_complete(delete_user(user_id))
    # test with a deleted user
    response = client.post("/users/access-token/", json={"username": TEST_USER_EMAIL, "password": TEST_USER_PASS})
    assert response.status_code == 404, response.text
    data = response.json()
    assert data["code"] == "not_found", data
    # test token of a deleted user
    response = client.get("/users/me/", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 401, response.text
    data = response.json()
    assert data["code"] == "invalid_credentials", data


def test_user_testing_tools(client: TestClient, event_loop: asyncio.AbstractEventLoop) -> None:
    # no exception for the delete function even if the user does not exist
    event_loop.run_until_complete(delete_user(32323))
    # no exception for id function even if the user with that email does not exist
    event_loop.run_until_complete(get_user_id_using_email(TEST_USER_EMAIL))
