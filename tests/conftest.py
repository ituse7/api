import asyncio
import os
from typing import Generator

import pytest
from fastapi.testclient import TestClient
from tortoise.contrib.test import finalizer, initializer


@pytest.fixture(scope="session")
def event_loop() -> Generator:
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def client(event_loop: asyncio.AbstractEventLoop) -> Generator:
    # set environment variables for the test environment
    os.environ["DATABASE_URI"] = "sqlite://:memory:"
    os.environ["DEBUG"] = "True"
    # import application generator
    from app.main import get_application

    # init database
    initializer(["app.models"])
    # return a test client
    app = get_application()
    with TestClient(app) as c:
        yield c
    # close db connection
    finalizer()
