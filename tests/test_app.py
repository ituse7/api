import pytest
from fastapi.testclient import TestClient

from app import __version__
from app.core.settings import Settings, settings
from app.schemas.common import HomepageResponseSchema
from app.schemas.exceptions import CommonExceptionSchema


def test_version():
    """
    Make sure we are upgrading the version before each release.
    Version should be updated in the app's init and in pyproject.toml file.
    """
    assert __version__ == "0.1.0"


def test_settings_model() -> None:
    base_settings = settings.dict()
    base_settings["CORS_ORIGINS"] = ""
    with pytest.raises(ValueError):
        Settings(**base_settings)
    base_settings["CORS_ORIGINS"] = 1
    with pytest.raises(ValueError):
        Settings(**base_settings)


def test_homepage(client: TestClient) -> None:
    response = client.get("/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == HomepageResponseSchema().dict()


def test_non_existing_endpoint(client: TestClient) -> None:
    response = client.get("/unknown_endpoint/")
    assert response.status_code == 404, response.text
    data = response.json()
    assert data == CommonExceptionSchema(message="Not Found", code="not_found").dict()
