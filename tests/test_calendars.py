# mypy: no-disallow-untyped-decorators

import asyncio

from fastapi.testclient import TestClient

from tests.shared_utils import delete_calendar_by_slug, delete_user, get_test_user_token

TEST_CAL_NAME = "TestCal"
TEST_CAL_SLUG = "testcal"


def test_create_calendar(client: TestClient, event_loop: asyncio.AbstractEventLoop) -> None:  # nosec
    # user and token
    user_id, token = get_test_user_token(client, event_loop)

    # check exception for the default calendar without one
    response = client.get("/calendars/default/", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 404, response.text
    data = response.json()
    assert "code" in data, data
    assert data["code"] == "not_found", data

    # incorrect timezone
    response = client.post(
        "/calendars/",
        json={
            "name": TEST_CAL_NAME,
            "slug": TEST_CAL_SLUG,
            "open_time": "10:30",
            "close_time": "16:30",
            "work_days": "Monday,Wednesday,Friday",
            "timezone": "Europe/Istanblue",
            "default_duration": 30,
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == 422, response.text
    data = response.json()
    assert "detail" in data, data
    assert len(data["detail"]) == 1, data
    assert data["detail"][0]["type"] == "value_error", data["detail"]

    # create the calendar
    response = client.post(
        "/calendars/",
        json={
            "name": TEST_CAL_NAME,
            "slug": TEST_CAL_SLUG,
            "open_time": "10:30",
            "close_time": "16:30",
            "work_days": "Monday,Wednesday,Friday",
            "timezone": "Europe/Istanbul",
            "default_duration": 30,
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["slug"] == TEST_CAL_SLUG, data

    # calendar exists exception
    response = client.post(
        "/calendars/",
        json={
            "name": "Another name",
            "slug": TEST_CAL_SLUG,
            "open_time": "10:30",
            "close_time": "16:30",
            "work_days": "Monday,Friday",
            "timezone": "Europe/Istanbul",
            "default_duration": 30,
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == 409, response.text
    data = response.json()
    assert data["code"] == "calendar_exists", data

    # list of calendars
    response = client.get("/calendars/", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 200, response.text
    data = response.json()
    assert len(data), data
    assert data[0]["slug"] == TEST_CAL_SLUG, data[0]

    # get the default user calendar
    response = client.get("/calendars/default/", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 200, response.text
    data = response.json()
    assert "slug" in data, data
    assert data["slug"] == TEST_CAL_SLUG, data

    # get public calendar info
    response = client.get(f"/calendars/{TEST_CAL_SLUG}/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["name"] == TEST_CAL_NAME, data
    assert "id" not in data, data

    # delete the test user
    event_loop.run_until_complete(delete_calendar_by_slug(TEST_CAL_SLUG))
    event_loop.run_until_complete(delete_user(user_id))

    # check public calendar info after deleting it
    response = client.get(f"/calendars/{TEST_CAL_SLUG}/")
    assert response.status_code == 404, response.text
    data = response.json()
    assert data["code"] == "not_found", data


def test_calendar_testing_tools(client: TestClient, event_loop: asyncio.AbstractEventLoop) -> None:
    # no exception for the delete function even if the calendar does not exist
    event_loop.run_until_complete(delete_calendar_by_slug(TEST_CAL_SLUG))
