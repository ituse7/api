import asyncio
from typing import Optional, Tuple

from fastapi.testclient import TestClient
from tortoise.exceptions import DoesNotExist

from app.models import Calendar, User

TEST_USER_EMAIL = "tester@debugwith.me"
TEST_USER_NAME = "Test User"
TEST_USER_PASS = "testpass"


async def get_user_id_using_email(email: str) -> Optional[int]:
    """Get user id value from db using email address."""
    try:
        user = await User.get(email=email)
    except DoesNotExist:
        return None
    return user.id


async def get_user_from_db(user_id: int) -> User:
    """Get user model from the database."""
    user = await User.get(id=user_id)
    return user


async def delete_user(user_id: int) -> None:
    """Delete user from db."""
    try:
        user = await User.get(id=user_id)
    except DoesNotExist:
        return
    await user.delete()


async def promote_user(user_id: int) -> None:
    """Make user a superuser."""
    user = await User.get(id=user_id)
    user.is_superuser = True
    await user.save()


async def deactivate_user(user_id: int) -> None:
    """Make is_active False."""
    user = await User.get(id=user_id)
    user.is_active = False
    await user.save()


async def delete_calendar_by_slug(slug: str) -> None:
    """Delete a calendar by slug."""
    try:
        calendar = await Calendar.get(slug=slug)
    except DoesNotExist:
        return
    await calendar.delete()


def get_test_user_token(client: TestClient, event_loop: asyncio.AbstractEventLoop) -> Tuple[int, str]:
    response = client.post(
        "/users/", json={"email": TEST_USER_EMAIL, "password": TEST_USER_PASS, "name": TEST_USER_NAME}
    )
    data = response.json()
    user_id = event_loop.run_until_complete(get_user_id_using_email(data["email"]))
    assert user_id is not None
    # get user token
    response = client.post("/users/access-token/", json={"username": TEST_USER_EMAIL, "password": TEST_USER_PASS})
    data = response.json()
    return user_id, data["access_token"]
