# mypy: no-disallow-untyped-decorators

import asyncio
from datetime import datetime, timedelta, timezone

from fastapi.testclient import TestClient

from tests.shared_utils import delete_user, get_test_user_token

TEST_CAL_NAME = "TestCal"
TEST_CAL_SLUG = "testcal"


def test_create_meeting(client: TestClient, event_loop: asyncio.AbstractEventLoop) -> None:  # nosec
    # user and token
    user_id, token = get_test_user_token(client, event_loop)

    # create the calendar
    response = client.post(
        "/calendars/",
        json={
            "name": TEST_CAL_NAME,
            "slug": TEST_CAL_SLUG,
            "open_time": "10:30",
            "close_time": "16:30",
            "work_days": "Monday,Wednesday,Friday",
            "timezone": "Europe/Istanbul",
            "default_duration": 30,
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == 200, response.text

    # prepare meeting details
    start_dt = datetime(year=2020, month=4, day=8, hour=13, minute=00, tzinfo=timezone(timedelta(hours=3)))
    end_dt = start_dt + timedelta(minutes=45)

    # create a meeting
    response = client.post(
        "/meetings/",
        json={
            "calendar_slug": TEST_CAL_SLUG,
            "meeting_start": start_dt.strftime("%Y-%m-%dT%H:%M:%S%z"),
            "meeting_end": end_dt.strftime("%Y-%m-%dT%H:%M:%S%z"),
            "fullname": "Guest 1",
            "email": "guest1@debugwith.me",
            "subject": "Test meeting",
        },
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["fullname"] == "Guest 1", data
    assert data["approved"] is False, data["approved"]

    # get list of meeting
    response = client.get("/meetings/", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 200, response.text
    data = response.json()
    assert len(data) == 2, data
    names = [meeting["subject"] for meeting in data]
    assert "Leg time" in names, names

    # list of meetings with calendar filter
    response = client.get(f"/meetings/?calendar_slug={TEST_CAL_SLUG}", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 200, response.text
    data = response.json()
    assert len(data) == 2, data

    # list of meetings with incorrect calendar filter
    response = client.get("/meetings/?calendar_slug=nocaltest", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 404, response.text
    data = response.json()
    assert data["code"] == "not_found"

    # create a meeting with datetime format: 2021-01-26T13:00:00Z
    response = client.post(
        "/meetings/",
        json={
            "calendar_slug": TEST_CAL_SLUG,
            "meeting_start": (start_dt - timedelta(hours=1)).strftime("%Y-%m-%dT%H:%M:%SZ"),
            "meeting_end": (end_dt - timedelta(hours=1)).strftime("%Y-%m-%dT%H:%M:%SZ"),
            "fullname": "Guest 2",
            "email": "guest2@debugwith.me",
            "subject": "Test meeting",
        },
    )
    assert response.status_code == 200, response.text

    # get public busy hours
    response = client.get(
        f"/calendars/{TEST_CAL_SLUG}/spots/",
        json={
            "start_time": (start_dt - timedelta(days=1)).replace(hour=1, minute=0).strftime("%Y-%m-%dT%H:%M:%S%z"),
            "end_time": (end_dt + timedelta(days=1)).replace(hour=1, minute=0).strftime("%Y-%m-%dT%H:%M:%S%z"),
        },
    )
    assert response.status_code == 200, response.text
    data = response.json()
    # 2 meetings and 2 leg times
    assert len(data) == 4, data

    # basic public spot request without correct calendar slug
    response = client.get(
        "/calendars/nocaltest/spots/",
        json={
            "start_time": (start_dt - timedelta(days=1)).replace(hour=1, minute=0).strftime("%Y-%m-%dT%H:%M:%S%z"),
            "end_time": (end_dt + timedelta(days=1)).replace(hour=1, minute=0).strftime("%Y-%m-%dT%H:%M:%S%z"),
        },
    )
    assert response.status_code == 404, response.text

    # delete user and its related data
    event_loop.run_until_complete(delete_user(user_id))
